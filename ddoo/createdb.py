
import sqlite3

if __name__ == '__main__':
    sqlSession = sqlite3.connect("database.db")
    cursor = sqlSession.cursor()

    cursor.execute("CREATE TABLE message (message_id integer PRIMARY KEY, fullname varchar, passport varchar, text varchar, status integer);")
    cursor.execute("CREATE TABLE hearing (hear_id integer PRIMARY KEY, type varchar);")
    sqlSession.commit()

    cursor.close()
    sqlSession.close()