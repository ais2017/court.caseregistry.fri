
from gateway.dbase import DBase
from classes.message import Message
from classes.enums import *

class Component:

    def __init__(self):
        pass

    def createMessage(self, fullName, passport, text):
        base = DBase()
        base.addNewMessage(fullName, passport, text)

    def getMessages(self):
        base = DBase()
        messageList = base.getMessages()
        messageListText =[]
        for i in messageList:
            if i[4] == 1:
                messageListText.append((str(i[0]), i[1], i[2], i[3], "На рассмотрении") )
            elif i[4] == 2:
                messageListText.append((str(i[0]), i[1], i[2], i[3], "Слушание запланировано"))
            elif i[4] == 3:
                messageListText.append((str(i[0]), i[1], i[2], i[3], "Дело открыто"))
            elif i[4] == 4:
                messageListText.append((str(i[0]), i[1], i[2], i[3], "Отказано"))

        return messageListText

    def sheduleMessage(self, id):
        base = DBase()
        messageCortage = base.getMessage(id)

        message = Message(messageCortage[1], messageCortage[0], messageCortage[2], messageCortage[3], Status(messageCortage[4]))
        hearing = message.shedule()

        if hearing:
            base.createHearing(hearing.getId(), hearing.getStripe())
            base.changeStatus(int(id), message.getStatus().value)


    def renounceMessage(self, id):
        base = DBase()
        messageCortage = base.getMessage(id)

        message = Message(messageCortage[1], messageCortage[0], messageCortage[2], messageCortage[3], Status(messageCortage[4]))
        message.refuse()

        base.changeStatus(id, message.getStatus().value)

    def getHearings(self):
        base = DBase()
        hearingsList = base.getHearings()
        hearingsListText = []
        for i in hearingsList:
            hearingsListText.append((str(i[0]), i[1]) )

        return hearingsListText