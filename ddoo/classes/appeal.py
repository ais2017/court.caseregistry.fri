# -*- coding: utf-8 -*-

from classes.enums import Status
from classes.hearing import Hearing

class Appeal:
    def __init__(self, fullName, number, passport, status):

        if not (type(fullName) is str):
            raise TypeError("Invalid init class Appeal: operand type of fullName is not str")
        if not (type(number) is int):
            raise TypeError("Invalid init class Appeal: operand type of number is not int")
        if not (type(passport) is str):
            raise TypeError("Invalid init class Appeal: operand type of passport is not str")
        if not (isinstance(status, Status)) and Status is not None:
            raise TypeError("Invalid init class Appeal: operand type of status is not Status")

        self._number = number                   # Номер обращения
        self._fullName = fullName              # ФИО  заявителя
        self._passport = passport               # Номер удостоверения
        self._status = status     # Статус

    def getNumber(self):
        return self._number

    def getFullName(self):
        return self._fullName

    def getPassport(self):
        return self._passport

    def getStatus(self):
        return self._status

    def refuse(self):
        if self._status == Status.considuration:
            self._status = Status.renouncement

    def shedule(self):

        if self._status == Status.considuration:
            self._status = Status.sheduling

            tempStripe = ""

            if self.__class__.__name__ == "Message":
                tempStripe = "Message about crime report"
            elif self.__class__.__name__ == "Statement":
                tempStripe = "Statement"

            tempHearing = Hearing(self._number, tempStripe)

            return tempHearing