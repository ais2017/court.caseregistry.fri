# -*- coding: utf-8 -*-

class Hearing:

    def __init__(self, hearId, stripe):

        if not (type(hearId) is int):
            raise TypeError("Invalid init class Hearing: operand type of hearId is not int")
        if not (type(stripe) is str):
            raise TypeError("Invalid init class Hearing: operand type of stripe is not int")

        self._id = hearId
        self._stripe = stripe

    def getId(self):
        return self._id

    def getStripe(self):
        return self._stripe


  #   Слушание  Hearing
  #   номер : число
  #   тип : строка