from enum import Enum

class TypeofCharge(Enum):
    private = 1
    private_public = 2
    public = 3

class Gravity(Enum):
    mild = 1
    moderate = 2
    severe = 3
    very_severe = 4

class Status(Enum):
    considuration = 1
    sheduling = 2
    opened = 3
    renouncement = 4