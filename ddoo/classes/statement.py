# -*- coding: utf-8 -*-

from classes.appeal import Appeal
from classes.enums import TypeofCharge, Gravity

class Statement(Appeal):

    def __init__(self, stateId, stateType, gravity):

        if not (type(stateId) is int):
            raise TypeError("Invalid init class Statement: operand type of id is not integer")

        if not (isinstance(stateType, TypeofCharge)):
            raise TypeError("Invalid init class Statement: operand type of stateType is not TypeOfCharge")

        if not (isinstance(gravity, Gravity)):
            raise TypeError("Invalid init class Statement: operand type of gravity is not Gravity")

        self._id = stateId
        self._type = stateType
        self._gravity = gravity


    def getId(self):
        return self._id

    def getType(self):
     return self._type

    def getGravity(self):
        return self._gravity