# -*- coding: utf-8 -*-

class Case:

     def __init__(self, caseId, fullNameJudge):

        if not (type(caseId) is int):
            raise TypeError("Invalid init class Case: operand type of caseId is not int")
        if not (type(fullNameJudge) is str):
            raise TypeError("Invalid init class Case: operand type of fullNameJudge is not str")

#        if not (isinstance(Verdict, Verdict ))and Verdict is not None:
#            raise TypeError("Invalid init class Statement: operand type of verdict is not None")
#        if not (isinstance(state) is string ):
#            raise TypeError("Invalid init class Statement: operand  of id is not state")

        self._id = caseId
        self._fullNameJudge = fullNameJudge

     def getId(self):
        return self._id

     def getFullNameJudge(self):
        return self._fullNameJudge