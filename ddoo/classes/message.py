# -*- coding: utf-8 -*-

from classes.appeal import Appeal

class Message(Appeal):

    def __init__(self, fullName, number, passport, messageText, status):

        if not (type(messageText) is str):
            raise TypeError("Invalid init class Message: operand type of messageText is not str")

        Appeal.__init__(self, fullName, number, passport, status)

        self._messageText = messageText

    def getMessageText(self):
        return self._messageText
