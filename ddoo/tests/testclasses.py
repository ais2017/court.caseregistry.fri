# -*- coding: utf-8 -*-

import unittest

from ddoo.classes.enums import *

from ddoo.classes.hearing import Hearing
from ddoo.classes.message import Message


class TestHearingClass(unittest.TestCase):
    def test_initHearing(self):
        a = Hearing(7, "Заявление, описание, много")
        self.assertEqual(a.getId(), 7)
        self.assertEqual(a.getStripe(), "Заявление, описание, много")

        with self.assertRaises(TypeError):
            b1 = Message("7", "Заявление, описание, много")

        with self.assertRaises(TypeError):
            b2 = Message(7, 123)

class TestMessageClass(unittest.TestCase):
    def test_initMessage(self):
        a = Message("Абрагимов Жанадил Дмитриевич", 1, "28 17 382710", "Полное преступление", Status.considuration)
        self.assertEqual(a.getFullName(), "Абрагимов Жанадил Дмитриевич")
        self.assertEqual(a.getNumber(), 1)
        self.assertEqual(a.getPassport(), "28 17 382710")
        self.assertEqual(a.getMessageText(), "Полное преступление")
        self.assertEqual(a.getStatus(), Status.considuration)

        with self.assertRaises(TypeError):
            b1 = Message(123, 1, "28 17 382710", "Полное преступление")

        with self.assertRaises(TypeError):
            b2 = Message("Абрагимов Жанадил Дмитриевич", "1-2", "28 17 382710", "Полное преступление")

        with self.assertRaises(TypeError):
            b3 = Message("Абрагимов Жанадил Дмитриевич", 1, 382710, "Полное преступление")

        with self.assertRaises(TypeError):
            b4 = Message("Абрагимов Жанадил Дмитриевич", 1, "28 17 382710", 10)

    def test_refuse(self):
        a = Message("Абрагимов Жанадил Дмитриевич", 1, "28 17 382710", "Полное преступление", Status.considuration)
        a.refuse()
        self.assertEqual(a.getStatus(), Status.renouncement)

        b = Message("Абрагимов Жанадил Дмитриевич", 1, "28 17 382710", "Полное преступление", Status.sheduling)
        b.refuse()
        self.assertEqual(b.getStatus(), Status.sheduling)

    def test_shedule(self):
        a = Message("Абрагимов Жанадил Дмитриевич", 8, "28 17 382710", "Полное преступление", Status.considuration)
        h = a.shedule()
        self.assertEqual(a.getStatus(), Status.sheduling)

        self.assertEqual( h.getId(), 8)
        self.assertEqual( h.getStripe(), "Message about crime report")


if __name__ == '__main__':
    unittest.main()
