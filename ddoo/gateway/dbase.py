
import sqlite3
from classes.enums import Status

class DBase:

    def __init__(self):
        pass

    def addNewMessage(self, fullName, passport, text):
        state = Status.considuration

        sqlSession = sqlite3.connect("database.db")
        cursor = sqlSession.cursor()

        cursor.execute("SELECT MAX(message_id) FROM message")
        idMaxList = cursor.fetchall()
        if (idMaxList[0][0]):
            maxId = idMaxList[0][0]
        else:
            maxId = 0;

        print(idMaxList)
        print(maxId)
        cursor.execute("INSERT INTO message (message_id, fullname, passport, text, status) VALUES (?, ?, ?, ?, ?)", (maxId+1, fullName, passport, text, 1))
        sqlSession.commit()

        cursor.close()
        sqlSession.close()

    def getMessages(self):
        sqlSession = sqlite3.connect("database.db")
        cursor = sqlSession.cursor()

        cursor.execute("SELECT message_id, fullname, passport, text, status FROM message")
        messageList = cursor.fetchall()

        cursor.close()
        sqlSession.close()

        return messageList

    def getMessage(self, message_id):
        sqlSession = sqlite3.connect("database.db")
        cursor = sqlSession.cursor()

        cursor.execute("SELECT message_id, fullname, passport, text, status FROM message WHERE message_id=?", (message_id,))
        messageList = cursor.fetchall()
        message = messageList[0]

        cursor.close()
        sqlSession.close()

        return message

    def changeStatus(self, message_id, newStatus):

        sqlSession = sqlite3.connect("database.db")
        cursor = sqlSession.cursor()

        print("UPDATING:")
        print(message_id)
        print(newStatus)
        cursor.execute("UPDATE message SET status=? WHERE message_id=?;", (newStatus, message_id))
        sqlSession.commit()

        cursor.close()
        sqlSession.close()

    def createHearing(self, hear_id, typeText):

        sqlSession = sqlite3.connect("database.db")
        cursor = sqlSession.cursor()

        cursor.execute("INSERT INTO hearing (hear_id, type) VALUES (?, ?)", (hear_id, typeText))
        sqlSession.commit()

        cursor.close()
        sqlSession.close()

    def getHearings(self):
        sqlSession = sqlite3.connect("database.db")
        cursor = sqlSession.cursor()

        cursor.execute("SELECT hear_id, type FROM hearing")
        hearingList = cursor.fetchall()

        print (hearingList)
        cursor.close()
        sqlSession.close()

        return hearingList
