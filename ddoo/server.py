from bottle import request, route, run
from component.component import Component

@route('/')
@route('/index.html', method='GET')
def index():
    page = '''
        <p><b>Действия</b></p>
        <a href="registration.html">Сообщение о преступлении</a>
        <a href="status.html">Состояние сообщений</a>
        <a href="hearing.html">Заседания</a>
    '''
    return page

@route('/registration.html', method='GET')
def registrationGET():
    page = '''<html>
  <body>
    <form method="post">
      <input type="text" name="fullname" placeholder="Введите имя" />
      <input type="text" name="passport" placeholder="Паспортные данные" />
      <input type="text" name="text" placeholder="Текст сообщения" />
      <input type="submit" value="Создать" />
    </form>
    <a href="/">Главная</a>
  </body>
</html>
'''
    return page


@route('/registration.html', method='POST')
def registrationPOST():
    fullname = request.POST.get('fullname')
    passport = request.POST.get('passport')
    text = request.POST.get('text')

    component = Component()
    component.createMessage(fullname, passport, text)
    page = "Created"
    page += '''<a href="/">Главная</a>'''
    return page


@route('/status.html', method='GET')
def statusGET():
    component = Component()
    messagesList = component.getMessages()

    page = '''
    <p><b>Заседания</b></p>
    <table border="1">
    '''

    page += '''
    <tr><td>ID</td><td>Полное имя</td><td>Паспортные данные</td><td>Текст сообщения</td><td>Статус</td></tr>
    '''

    for i in messagesList:
        page += "<tr>"
        for j in i:
            page += "<td>"
            page += j
            page += "</td>"
        page += "</tr>"

    page += '''</table>'''

    page += '''
    <form method="post">
      <input type="text" name="idShedule" placeholder="Введите id" />      
      <input type="submit" value="Назначить слушание" />
    </form>
    <form method="post">
      <input type="text" name="idRenounce" placeholder="Введите id" />      
      <input type="submit" value="Отказать" />
    </form>
    '''

    page += '''<a href="/">Главная</a>'''

    return page


@route('/status.html', method='POST')
def statusPOST():
    idShedule = request.POST.get('idShedule')
    idRenounce = request.POST.get('idRenounce')

    component = Component()

    if idShedule:
        component.sheduleMessage(idShedule)
        return "Sheduled"+'''<a href="/">Главная</a>'''
    elif idRenounce:
        component.renounceMessage(idRenounce)
        return "Renounced"+'''<a href="/">Главная</a>'''


@route('/hearing.html', method='GET')
def hearingGET():
    component = Component()
    hearingList = component.getHearings()

    page = '''
    <p><b>Сообщения</b></p>
    <table border="1">
    '''

    page += '''
    <tr><td>Номер</td><td>Тип</td></tr>
    '''

    for i in hearingList:
        page += "<tr>"
        for j in i:
            page += "<td>"
            print("j")
            print(j)
            page += j
            page += "</td>"
        page += "</tr>"

    page += '''</table>'''

    page += '''<a href="/">Главная</a>'''

    return page

if __name__ == '__main__':
    run(host='localhost', port=8080)